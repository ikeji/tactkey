EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:tactkey-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SW_Push SW1
U 1 1 5BD05B7B
P 1200 3750
F 0 "SW1" H 1250 3850 50  0000 L CNN
F 1 "SW_Push" H 1200 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1200 3950 50  0001 C CNN
F 3 "" H 1200 3950 50  0001 C CNN
	1    1200 3750
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 5BD05BD2
P 1450 3600
F 0 "D1" H 1450 3700 50  0000 C CNN
F 1 "D" H 1450 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 1450 3600 50  0001 C CNN
F 3 "" H 1450 3600 50  0001 C CNN
	1    1450 3600
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x06 J1
U 1 1 5BD05CE7
P 1400 2750
F 0 "J1" H 1400 3050 50  0000 C CNN
F 1 "Conn_01x06" H 1400 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 1400 2750 50  0001 C CNN
F 3 "" H 1400 2750 50  0001 C CNN
	1    1400 2750
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW7
U 1 1 5BD069A6
P 1850 3750
F 0 "SW7" H 1900 3850 50  0000 L CNN
F 1 "SW_Push" H 1850 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1850 3950 50  0001 C CNN
F 3 "" H 1850 3950 50  0001 C CNN
	1    1850 3750
	1    0    0    -1  
$EndComp
$Comp
L D D7
U 1 1 5BD069AC
P 2100 3600
F 0 "D7" H 2100 3700 50  0000 C CNN
F 1 "D" H 2100 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2100 3600 50  0001 C CNN
F 3 "" H 2100 3600 50  0001 C CNN
	1    2100 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW11
U 1 1 5BD06D6F
P 2500 3750
F 0 "SW11" H 2550 3850 50  0000 L CNN
F 1 "SW_Push" H 2500 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 2500 3950 50  0001 C CNN
F 3 "" H 2500 3950 50  0001 C CNN
	1    2500 3750
	1    0    0    -1  
$EndComp
$Comp
L D D11
U 1 1 5BD06D75
P 2750 3600
F 0 "D11" H 2750 3700 50  0000 C CNN
F 1 "D" H 2750 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2750 3600 50  0001 C CNN
F 3 "" H 2750 3600 50  0001 C CNN
	1    2750 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW14
U 1 1 5BD06D7C
P 3150 3750
F 0 "SW14" H 3200 3850 50  0000 L CNN
F 1 "SW_Push" H 3150 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3150 3950 50  0001 C CNN
F 3 "" H 3150 3950 50  0001 C CNN
	1    3150 3750
	1    0    0    -1  
$EndComp
$Comp
L D D14
U 1 1 5BD06D82
P 3400 3600
F 0 "D14" H 3400 3700 50  0000 C CNN
F 1 "D" H 3400 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 3400 3600 50  0001 C CNN
F 3 "" H 3400 3600 50  0001 C CNN
	1    3400 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW31
U 1 1 5BD07033
P 6450 3750
F 0 "SW31" H 6500 3850 50  0000 L CNN
F 1 "SW_Push" H 6450 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 6450 3950 50  0001 C CNN
F 3 "" H 6450 3950 50  0001 C CNN
	1    6450 3750
	1    0    0    -1  
$EndComp
$Comp
L D D31
U 1 1 5BD07039
P 6700 3600
F 0 "D31" H 6700 3700 50  0000 C CNN
F 1 "D" H 6700 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6700 3600 50  0001 C CNN
F 3 "" H 6700 3600 50  0001 C CNN
	1    6700 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW34
U 1 1 5BD07040
P 7100 3750
F 0 "SW34" H 7150 3850 50  0000 L CNN
F 1 "SW_Push" H 7100 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7100 3950 50  0001 C CNN
F 3 "" H 7100 3950 50  0001 C CNN
	1    7100 3750
	1    0    0    -1  
$EndComp
$Comp
L D D34
U 1 1 5BD07046
P 7350 3600
F 0 "D34" H 7350 3700 50  0000 C CNN
F 1 "D" H 7350 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 7350 3600 50  0001 C CNN
F 3 "" H 7350 3600 50  0001 C CNN
	1    7350 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW37
U 1 1 5BD0704D
P 7750 3750
F 0 "SW37" H 7800 3850 50  0000 L CNN
F 1 "SW_Push" H 7750 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7750 3950 50  0001 C CNN
F 3 "" H 7750 3950 50  0001 C CNN
	1    7750 3750
	1    0    0    -1  
$EndComp
$Comp
L D D37
U 1 1 5BD07053
P 8000 3600
F 0 "D37" H 8000 3700 50  0000 C CNN
F 1 "D" H 8000 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8000 3600 50  0001 C CNN
F 3 "" H 8000 3600 50  0001 C CNN
	1    8000 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW40
U 1 1 5BD0705A
P 8400 3750
F 0 "SW40" H 8450 3850 50  0000 L CNN
F 1 "SW_Push" H 8400 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 8400 3950 50  0001 C CNN
F 3 "" H 8400 3950 50  0001 C CNN
	1    8400 3750
	1    0    0    -1  
$EndComp
$Comp
L D D40
U 1 1 5BD07060
P 8650 3600
F 0 "D40" H 8650 3700 50  0000 C CNN
F 1 "D" H 8650 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8650 3600 50  0001 C CNN
F 3 "" H 8650 3600 50  0001 C CNN
	1    8650 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW17
U 1 1 5BD074CB
P 3800 3750
F 0 "SW17" H 3850 3850 50  0000 L CNN
F 1 "SW_Push" H 3800 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3800 3950 50  0001 C CNN
F 3 "" H 3800 3950 50  0001 C CNN
	1    3800 3750
	1    0    0    -1  
$EndComp
$Comp
L D D17
U 1 1 5BD074D1
P 4050 3600
F 0 "D17" H 4050 3700 50  0000 C CNN
F 1 "D" H 4050 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4050 3600 50  0001 C CNN
F 3 "" H 4050 3600 50  0001 C CNN
	1    4050 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW20
U 1 1 5BD074D8
P 4450 3750
F 0 "SW20" H 4500 3850 50  0000 L CNN
F 1 "SW_Push" H 4450 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4450 3950 50  0001 C CNN
F 3 "" H 4450 3950 50  0001 C CNN
	1    4450 3750
	1    0    0    -1  
$EndComp
$Comp
L D D20
U 1 1 5BD074DE
P 4700 3600
F 0 "D20" H 4700 3700 50  0000 C CNN
F 1 "D" H 4700 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4700 3600 50  0001 C CNN
F 3 "" H 4700 3600 50  0001 C CNN
	1    4700 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW24
U 1 1 5BD074E5
P 5100 3750
F 0 "SW24" H 5150 3850 50  0000 L CNN
F 1 "SW_Push" H 5100 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5100 3950 50  0001 C CNN
F 3 "" H 5100 3950 50  0001 C CNN
	1    5100 3750
	1    0    0    -1  
$EndComp
$Comp
L D D24
U 1 1 5BD074EB
P 5350 3600
F 0 "D24" H 5350 3700 50  0000 C CNN
F 1 "D" H 5350 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 5350 3600 50  0001 C CNN
F 3 "" H 5350 3600 50  0001 C CNN
	1    5350 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW28
U 1 1 5BD074F2
P 5750 3750
F 0 "SW28" H 5800 3850 50  0000 L CNN
F 1 "SW_Push" H 5750 3690 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5750 3950 50  0001 C CNN
F 3 "" H 5750 3950 50  0001 C CNN
	1    5750 3750
	1    0    0    -1  
$EndComp
$Comp
L D D28
U 1 1 5BD074F8
P 6000 3600
F 0 "D28" H 6000 3700 50  0000 C CNN
F 1 "D" H 6000 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6000 3600 50  0001 C CNN
F 3 "" H 6000 3600 50  0001 C CNN
	1    6000 3600
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW2
U 1 1 5BD077E9
P 1200 4250
F 0 "SW2" H 1250 4350 50  0000 L CNN
F 1 "SW_Push" H 1200 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1200 4450 50  0001 C CNN
F 3 "" H 1200 4450 50  0001 C CNN
	1    1200 4250
	1    0    0    -1  
$EndComp
$Comp
L D D2
U 1 1 5BD077EF
P 1450 4100
F 0 "D2" H 1450 4200 50  0000 C CNN
F 1 "D" H 1450 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 1450 4100 50  0001 C CNN
F 3 "" H 1450 4100 50  0001 C CNN
	1    1450 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW8
U 1 1 5BD077F6
P 1850 4250
F 0 "SW8" H 1900 4350 50  0000 L CNN
F 1 "SW_Push" H 1850 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1850 4450 50  0001 C CNN
F 3 "" H 1850 4450 50  0001 C CNN
	1    1850 4250
	1    0    0    -1  
$EndComp
$Comp
L D D8
U 1 1 5BD077FC
P 2100 4100
F 0 "D8" H 2100 4200 50  0000 C CNN
F 1 "D" H 2100 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2100 4100 50  0001 C CNN
F 3 "" H 2100 4100 50  0001 C CNN
	1    2100 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW12
U 1 1 5BD07803
P 2500 4250
F 0 "SW12" H 2550 4350 50  0000 L CNN
F 1 "SW_Push" H 2500 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 2500 4450 50  0001 C CNN
F 3 "" H 2500 4450 50  0001 C CNN
	1    2500 4250
	1    0    0    -1  
$EndComp
$Comp
L D D12
U 1 1 5BD07809
P 2750 4100
F 0 "D12" H 2750 4200 50  0000 C CNN
F 1 "D" H 2750 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2750 4100 50  0001 C CNN
F 3 "" H 2750 4100 50  0001 C CNN
	1    2750 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW15
U 1 1 5BD07810
P 3150 4250
F 0 "SW15" H 3200 4350 50  0000 L CNN
F 1 "SW_Push" H 3150 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3150 4450 50  0001 C CNN
F 3 "" H 3150 4450 50  0001 C CNN
	1    3150 4250
	1    0    0    -1  
$EndComp
$Comp
L D D15
U 1 1 5BD07816
P 3400 4100
F 0 "D15" H 3400 4200 50  0000 C CNN
F 1 "D" H 3400 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 3400 4100 50  0001 C CNN
F 3 "" H 3400 4100 50  0001 C CNN
	1    3400 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW32
U 1 1 5BD0781D
P 6450 4250
F 0 "SW32" H 6500 4350 50  0000 L CNN
F 1 "SW_Push" H 6450 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 6450 4450 50  0001 C CNN
F 3 "" H 6450 4450 50  0001 C CNN
	1    6450 4250
	1    0    0    -1  
$EndComp
$Comp
L D D32
U 1 1 5BD07823
P 6700 4100
F 0 "D32" H 6700 4200 50  0000 C CNN
F 1 "D" H 6700 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6700 4100 50  0001 C CNN
F 3 "" H 6700 4100 50  0001 C CNN
	1    6700 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW35
U 1 1 5BD0782A
P 7100 4250
F 0 "SW35" H 7150 4350 50  0000 L CNN
F 1 "SW_Push" H 7100 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7100 4450 50  0001 C CNN
F 3 "" H 7100 4450 50  0001 C CNN
	1    7100 4250
	1    0    0    -1  
$EndComp
$Comp
L D D35
U 1 1 5BD07830
P 7350 4100
F 0 "D35" H 7350 4200 50  0000 C CNN
F 1 "D" H 7350 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 7350 4100 50  0001 C CNN
F 3 "" H 7350 4100 50  0001 C CNN
	1    7350 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW38
U 1 1 5BD07837
P 7750 4250
F 0 "SW38" H 7800 4350 50  0000 L CNN
F 1 "SW_Push" H 7750 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7750 4450 50  0001 C CNN
F 3 "" H 7750 4450 50  0001 C CNN
	1    7750 4250
	1    0    0    -1  
$EndComp
$Comp
L D D38
U 1 1 5BD0783D
P 8000 4100
F 0 "D38" H 8000 4200 50  0000 C CNN
F 1 "D" H 8000 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8000 4100 50  0001 C CNN
F 3 "" H 8000 4100 50  0001 C CNN
	1    8000 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW41
U 1 1 5BD07844
P 8400 4250
F 0 "SW41" H 8450 4350 50  0000 L CNN
F 1 "SW_Push" H 8400 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 8400 4450 50  0001 C CNN
F 3 "" H 8400 4450 50  0001 C CNN
	1    8400 4250
	1    0    0    -1  
$EndComp
$Comp
L D D41
U 1 1 5BD0784A
P 8650 4100
F 0 "D41" H 8650 4200 50  0000 C CNN
F 1 "D" H 8650 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8650 4100 50  0001 C CNN
F 3 "" H 8650 4100 50  0001 C CNN
	1    8650 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW18
U 1 1 5BD07851
P 3800 4250
F 0 "SW18" H 3850 4350 50  0000 L CNN
F 1 "SW_Push" H 3800 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3800 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0001 C CNN
	1    3800 4250
	1    0    0    -1  
$EndComp
$Comp
L D D18
U 1 1 5BD07857
P 4050 4100
F 0 "D18" H 4050 4200 50  0000 C CNN
F 1 "D" H 4050 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4050 4100 50  0001 C CNN
F 3 "" H 4050 4100 50  0001 C CNN
	1    4050 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW21
U 1 1 5BD0785E
P 4450 4250
F 0 "SW21" H 4500 4350 50  0000 L CNN
F 1 "SW_Push" H 4450 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4450 4450 50  0001 C CNN
F 3 "" H 4450 4450 50  0001 C CNN
	1    4450 4250
	1    0    0    -1  
$EndComp
$Comp
L D D21
U 1 1 5BD07864
P 4700 4100
F 0 "D21" H 4700 4200 50  0000 C CNN
F 1 "D" H 4700 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4700 4100 50  0001 C CNN
F 3 "" H 4700 4100 50  0001 C CNN
	1    4700 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW25
U 1 1 5BD0786B
P 5100 4250
F 0 "SW25" H 5150 4350 50  0000 L CNN
F 1 "SW_Push" H 5100 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5100 4450 50  0001 C CNN
F 3 "" H 5100 4450 50  0001 C CNN
	1    5100 4250
	1    0    0    -1  
$EndComp
$Comp
L D D25
U 1 1 5BD07871
P 5350 4100
F 0 "D25" H 5350 4200 50  0000 C CNN
F 1 "D" H 5350 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 5350 4100 50  0001 C CNN
F 3 "" H 5350 4100 50  0001 C CNN
	1    5350 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW29
U 1 1 5BD07878
P 5750 4250
F 0 "SW29" H 5800 4350 50  0000 L CNN
F 1 "SW_Push" H 5750 4190 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5750 4450 50  0001 C CNN
F 3 "" H 5750 4450 50  0001 C CNN
	1    5750 4250
	1    0    0    -1  
$EndComp
$Comp
L D D29
U 1 1 5BD0787E
P 6000 4100
F 0 "D29" H 6000 4200 50  0000 C CNN
F 1 "D" H 6000 4000 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6000 4100 50  0001 C CNN
F 3 "" H 6000 4100 50  0001 C CNN
	1    6000 4100
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW3
U 1 1 5BD07B87
P 1200 4800
F 0 "SW3" H 1250 4900 50  0000 L CNN
F 1 "SW_Push" H 1200 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1200 5000 50  0001 C CNN
F 3 "" H 1200 5000 50  0001 C CNN
	1    1200 4800
	1    0    0    -1  
$EndComp
$Comp
L D D3
U 1 1 5BD07B8D
P 1450 4650
F 0 "D3" H 1450 4750 50  0000 C CNN
F 1 "D" H 1450 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 1450 4650 50  0001 C CNN
F 3 "" H 1450 4650 50  0001 C CNN
	1    1450 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW9
U 1 1 5BD07B94
P 1850 4800
F 0 "SW9" H 1900 4900 50  0000 L CNN
F 1 "SW_Push" H 1850 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1850 5000 50  0001 C CNN
F 3 "" H 1850 5000 50  0001 C CNN
	1    1850 4800
	1    0    0    -1  
$EndComp
$Comp
L D D9
U 1 1 5BD07B9A
P 2100 4650
F 0 "D9" H 2100 4750 50  0000 C CNN
F 1 "D" H 2100 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2100 4650 50  0001 C CNN
F 3 "" H 2100 4650 50  0001 C CNN
	1    2100 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW13
U 1 1 5BD07BA1
P 2500 4800
F 0 "SW13" H 2550 4900 50  0000 L CNN
F 1 "SW_Push" H 2500 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 2500 5000 50  0001 C CNN
F 3 "" H 2500 5000 50  0001 C CNN
	1    2500 4800
	1    0    0    -1  
$EndComp
$Comp
L D D13
U 1 1 5BD07BA7
P 2750 4650
F 0 "D13" H 2750 4750 50  0000 C CNN
F 1 "D" H 2750 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2750 4650 50  0001 C CNN
F 3 "" H 2750 4650 50  0001 C CNN
	1    2750 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW16
U 1 1 5BD07BAE
P 3150 4800
F 0 "SW16" H 3200 4900 50  0000 L CNN
F 1 "SW_Push" H 3150 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3150 5000 50  0001 C CNN
F 3 "" H 3150 5000 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L D D16
U 1 1 5BD07BB4
P 3400 4650
F 0 "D16" H 3400 4750 50  0000 C CNN
F 1 "D" H 3400 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 3400 4650 50  0001 C CNN
F 3 "" H 3400 4650 50  0001 C CNN
	1    3400 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW33
U 1 1 5BD07BBB
P 6450 4800
F 0 "SW33" H 6500 4900 50  0000 L CNN
F 1 "SW_Push" H 6450 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 6450 5000 50  0001 C CNN
F 3 "" H 6450 5000 50  0001 C CNN
	1    6450 4800
	1    0    0    -1  
$EndComp
$Comp
L D D33
U 1 1 5BD07BC1
P 6700 4650
F 0 "D33" H 6700 4750 50  0000 C CNN
F 1 "D" H 6700 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6700 4650 50  0001 C CNN
F 3 "" H 6700 4650 50  0001 C CNN
	1    6700 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW36
U 1 1 5BD07BC8
P 7100 4800
F 0 "SW36" H 7150 4900 50  0000 L CNN
F 1 "SW_Push" H 7100 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7100 5000 50  0001 C CNN
F 3 "" H 7100 5000 50  0001 C CNN
	1    7100 4800
	1    0    0    -1  
$EndComp
$Comp
L D D36
U 1 1 5BD07BCE
P 7350 4650
F 0 "D36" H 7350 4750 50  0000 C CNN
F 1 "D" H 7350 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 7350 4650 50  0001 C CNN
F 3 "" H 7350 4650 50  0001 C CNN
	1    7350 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW39
U 1 1 5BD07BD5
P 7750 4800
F 0 "SW39" H 7800 4900 50  0000 L CNN
F 1 "SW_Push" H 7750 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7750 5000 50  0001 C CNN
F 3 "" H 7750 5000 50  0001 C CNN
	1    7750 4800
	1    0    0    -1  
$EndComp
$Comp
L D D39
U 1 1 5BD07BDB
P 8000 4650
F 0 "D39" H 8000 4750 50  0000 C CNN
F 1 "D" H 8000 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8000 4650 50  0001 C CNN
F 3 "" H 8000 4650 50  0001 C CNN
	1    8000 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW42
U 1 1 5BD07BE2
P 8400 4800
F 0 "SW42" H 8450 4900 50  0000 L CNN
F 1 "SW_Push" H 8400 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 8400 5000 50  0001 C CNN
F 3 "" H 8400 5000 50  0001 C CNN
	1    8400 4800
	1    0    0    -1  
$EndComp
$Comp
L D D42
U 1 1 5BD07BE8
P 8650 4650
F 0 "D42" H 8650 4750 50  0000 C CNN
F 1 "D" H 8650 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 8650 4650 50  0001 C CNN
F 3 "" H 8650 4650 50  0001 C CNN
	1    8650 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW19
U 1 1 5BD07BEF
P 3800 4800
F 0 "SW19" H 3850 4900 50  0000 L CNN
F 1 "SW_Push" H 3800 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 3800 5000 50  0001 C CNN
F 3 "" H 3800 5000 50  0001 C CNN
	1    3800 4800
	1    0    0    -1  
$EndComp
$Comp
L D D19
U 1 1 5BD07BF5
P 4050 4650
F 0 "D19" H 4050 4750 50  0000 C CNN
F 1 "D" H 4050 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4050 4650 50  0001 C CNN
F 3 "" H 4050 4650 50  0001 C CNN
	1    4050 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW22
U 1 1 5BD07BFC
P 4450 4800
F 0 "SW22" H 4500 4900 50  0000 L CNN
F 1 "SW_Push" H 4450 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4450 5000 50  0001 C CNN
F 3 "" H 4450 5000 50  0001 C CNN
	1    4450 4800
	1    0    0    -1  
$EndComp
$Comp
L D D22
U 1 1 5BD07C02
P 4700 4650
F 0 "D22" H 4700 4750 50  0000 C CNN
F 1 "D" H 4700 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4700 4650 50  0001 C CNN
F 3 "" H 4700 4650 50  0001 C CNN
	1    4700 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW26
U 1 1 5BD07C09
P 5100 4800
F 0 "SW26" H 5150 4900 50  0000 L CNN
F 1 "SW_Push" H 5100 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5100 5000 50  0001 C CNN
F 3 "" H 5100 5000 50  0001 C CNN
	1    5100 4800
	1    0    0    -1  
$EndComp
$Comp
L D D26
U 1 1 5BD07C0F
P 5350 4650
F 0 "D26" H 5350 4750 50  0000 C CNN
F 1 "D" H 5350 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 5350 4650 50  0001 C CNN
F 3 "" H 5350 4650 50  0001 C CNN
	1    5350 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW30
U 1 1 5BD07C16
P 5750 4800
F 0 "SW30" H 5800 4900 50  0000 L CNN
F 1 "SW_Push" H 5750 4740 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5750 5000 50  0001 C CNN
F 3 "" H 5750 5000 50  0001 C CNN
	1    5750 4800
	1    0    0    -1  
$EndComp
$Comp
L D D30
U 1 1 5BD07C1C
P 6000 4650
F 0 "D30" H 6000 4750 50  0000 C CNN
F 1 "D" H 6000 4550 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 6000 4650 50  0001 C CNN
F 3 "" H 6000 4650 50  0001 C CNN
	1    6000 4650
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW5
U 1 1 5BD0EAAC
P 1750 1300
F 0 "SW5" H 1800 1400 50  0000 L CNN
F 1 "SW_Push" H 1750 1240 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1750 1500 50  0001 C CNN
F 3 "" H 1750 1500 50  0001 C CNN
	1    1750 1300
	1    0    0    -1  
$EndComp
$Comp
L D D5
U 1 1 5BD0EAB2
P 2000 1150
F 0 "D5" H 2000 1250 50  0000 C CNN
F 1 "D" H 2000 1050 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2000 1150 50  0001 C CNN
F 3 "" H 2000 1150 50  0001 C CNN
	1    2000 1150
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW23
U 1 1 5BD0EFC2
P 4650 1700
F 0 "SW23" H 4700 1800 50  0000 L CNN
F 1 "SW_Push" H 4650 1640 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 4650 1900 50  0001 C CNN
F 3 "" H 4650 1900 50  0001 C CNN
	1    4650 1700
	1    0    0    -1  
$EndComp
$Comp
L D D23
U 1 1 5BD0EFC8
P 4900 1550
F 0 "D23" H 4900 1650 50  0000 C CNN
F 1 "D" H 4900 1450 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 4900 1550 50  0001 C CNN
F 3 "" H 4900 1550 50  0001 C CNN
	1    4900 1550
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW6
U 1 1 5BD0F177
P 1750 2300
F 0 "SW6" H 1800 2400 50  0000 L CNN
F 1 "SW_Push" H 1750 2240 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1750 2500 50  0001 C CNN
F 3 "" H 1750 2500 50  0001 C CNN
	1    1750 2300
	1    0    0    -1  
$EndComp
$Comp
L D D6
U 1 1 5BD0F17D
P 2000 2150
F 0 "D6" H 2000 2250 50  0000 C CNN
F 1 "D" H 2000 2050 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2000 2150 50  0001 C CNN
F 3 "" H 2000 2150 50  0001 C CNN
	1    2000 2150
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW4
U 1 1 5BD0F422
P 1300 1850
F 0 "SW4" H 1350 1950 50  0000 L CNN
F 1 "SW_Push" H 1300 1790 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 1300 2050 50  0001 C CNN
F 3 "" H 1300 2050 50  0001 C CNN
	1    1300 1850
	1    0    0    -1  
$EndComp
$Comp
L D D4
U 1 1 5BD0F428
P 1550 1700
F 0 "D4" H 1550 1800 50  0000 C CNN
F 1 "D" H 1550 1600 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 1550 1700 50  0001 C CNN
F 3 "" H 1550 1700 50  0001 C CNN
	1    1550 1700
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW10
U 1 1 5BD0F693
P 2300 1900
F 0 "SW10" H 2350 2000 50  0000 L CNN
F 1 "SW_Push" H 2300 1840 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 2300 2100 50  0001 C CNN
F 3 "" H 2300 2100 50  0001 C CNN
	1    2300 1900
	1    0    0    -1  
$EndComp
$Comp
L D D10
U 1 1 5BD0F699
P 2550 1750
F 0 "D10" H 2550 1850 50  0000 C CNN
F 1 "D" H 2550 1650 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1750
	0    -1   -1   0   
$EndComp
$Comp
L SW_Push SW27
U 1 1 5BD0F764
P 5400 1550
F 0 "SW27" H 5450 1650 50  0000 L CNN
F 1 "SW_Push" H 5400 1490 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 5400 1750 50  0001 C CNN
F 3 "" H 5400 1750 50  0001 C CNN
	1    5400 1550
	1    0    0    -1  
$EndComp
$Comp
L D D27
U 1 1 5BD0F76A
P 5650 1400
F 0 "D27" H 5650 1500 50  0000 C CNN
F 1 "D" H 5650 1300 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 5650 1400 50  0001 C CNN
F 3 "" H 5650 1400 50  0001 C CNN
	1    5650 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1400 3750 1450 3750
Wire Wire Line
	2050 3750 2100 3750
Wire Wire Line
	2700 3750 2750 3750
Wire Wire Line
	3350 3750 3400 3750
Wire Wire Line
	6650 3750 6700 3750
Wire Wire Line
	7300 3750 7350 3750
Wire Wire Line
	7950 3750 8000 3750
Wire Wire Line
	8600 3750 8650 3750
Wire Wire Line
	4000 3750 4050 3750
Wire Wire Line
	4650 3750 4700 3750
Wire Wire Line
	5300 3750 5350 3750
Wire Wire Line
	5950 3750 6000 3750
Wire Wire Line
	1400 4250 1450 4250
Wire Wire Line
	2050 4250 2100 4250
Wire Wire Line
	2700 4250 2750 4250
Wire Wire Line
	3350 4250 3400 4250
Wire Wire Line
	6650 4250 6700 4250
Wire Wire Line
	7300 4250 7350 4250
Wire Wire Line
	7950 4250 8000 4250
Wire Wire Line
	8600 4250 8650 4250
Wire Wire Line
	4000 4250 4050 4250
Wire Wire Line
	4650 4250 4700 4250
Wire Wire Line
	5300 4250 5350 4250
Wire Wire Line
	5950 4250 6000 4250
Wire Wire Line
	1400 4800 1450 4800
Wire Wire Line
	2050 4800 2100 4800
Wire Wire Line
	2700 4800 2750 4800
Wire Wire Line
	3350 4800 3400 4800
Wire Wire Line
	6650 4800 6700 4800
Wire Wire Line
	7300 4800 7350 4800
Wire Wire Line
	7950 4800 8000 4800
Wire Wire Line
	8600 4800 8650 4800
Wire Wire Line
	4000 4800 4050 4800
Wire Wire Line
	4650 4800 4700 4800
Wire Wire Line
	5300 4800 5350 4800
Wire Wire Line
	5950 4800 6000 4800
Wire Wire Line
	1200 2950 1000 2950
Wire Wire Line
	1000 2950 1000 5000
Connection ~ 1000 3750
Connection ~ 1000 4250
Wire Wire Line
	1300 2000 1300 3300
Wire Wire Line
	1300 3300 1650 3300
Wire Wire Line
	1650 3300 1650 5050
Connection ~ 1650 3750
Connection ~ 1650 4250
Wire Wire Line
	1400 2300 1400 3200
Wire Wire Line
	1400 3200 2300 3200
Wire Wire Line
	2300 3200 2300 5100
Connection ~ 2300 3750
Connection ~ 2300 4250
Wire Wire Line
	1500 2450 1500 3150
Wire Wire Line
	1500 3150 2950 3150
Wire Wire Line
	2950 3150 2950 5150
Connection ~ 2950 3750
Connection ~ 2950 4250
Wire Wire Line
	1600 2500 1600 3100
Wire Wire Line
	1600 3100 3600 3100
Wire Wire Line
	3600 3100 3600 5200
Connection ~ 3600 3750
Connection ~ 3600 4250
Wire Wire Line
	1700 2550 1700 3050
Wire Wire Line
	1700 3050 4250 3050
Wire Wire Line
	4250 3050 4250 5250
Connection ~ 4250 3750
Connection ~ 4250 4250
Wire Wire Line
	1000 5000 4900 5000
Wire Wire Line
	4900 5000 4900 3750
Connection ~ 1000 4800
Connection ~ 4900 4800
Connection ~ 4900 4250
Wire Wire Line
	1650 5050 5550 5050
Wire Wire Line
	5550 5050 5550 3750
Connection ~ 1650 4800
Connection ~ 5550 4800
Connection ~ 5550 4250
Wire Wire Line
	2300 5100 6250 5100
Wire Wire Line
	6250 5100 6250 3750
Connection ~ 2300 4800
Connection ~ 6250 4800
Connection ~ 6250 4250
Wire Wire Line
	2950 5150 6900 5150
Wire Wire Line
	6900 5150 6900 3750
Connection ~ 2950 4800
Connection ~ 6900 4800
Connection ~ 6900 4250
Wire Wire Line
	7550 3750 7550 5200
Connection ~ 7550 4250
Wire Wire Line
	8200 3750 8200 5250
Connection ~ 8200 4250
Wire Wire Line
	7550 5200 3600 5200
Connection ~ 3600 4800
Connection ~ 7550 4800
Wire Wire Line
	8200 5250 4250 5250
Connection ~ 8200 4800
Connection ~ 4250 4800
Wire Wire Line
	4400 2900 4400 3350
Wire Wire Line
	4400 3350 4700 3350
Wire Wire Line
	4700 3350 4700 3450
Wire Wire Line
	4700 3450 1450 3450
Connection ~ 4050 3450
Connection ~ 3400 3450
Connection ~ 2750 3450
Connection ~ 2100 3450
Wire Wire Line
	4500 2900 4500 3300
Wire Wire Line
	4500 3300 4750 3300
Wire Wire Line
	4750 3300 4750 3950
Wire Wire Line
	4750 3950 1450 3950
Connection ~ 4700 3950
Connection ~ 4050 3950
Connection ~ 3400 3950
Connection ~ 2750 3950
Connection ~ 2100 3950
Wire Wire Line
	1450 4500 4800 4500
Connection ~ 2100 4500
Connection ~ 2750 4500
Connection ~ 3400 4500
Connection ~ 4050 4500
Wire Wire Line
	4800 4500 4800 3200
Wire Wire Line
	4800 3200 4600 3200
Wire Wire Line
	4600 3200 4600 2900
Connection ~ 4700 4500
Wire Wire Line
	4900 2900 4900 3050
Wire Wire Line
	4900 3050 5350 3050
Wire Wire Line
	5350 3050 5350 3450
Wire Wire Line
	5350 3450 8650 3450
Connection ~ 6000 3450
Connection ~ 6700 3450
Connection ~ 7350 3450
Connection ~ 8000 3450
Wire Wire Line
	4800 2900 4800 3100
Wire Wire Line
	4800 3100 5300 3100
Wire Wire Line
	5300 3100 5300 3950
Wire Wire Line
	5300 3950 8650 3950
Connection ~ 5350 3950
Connection ~ 6000 3950
Connection ~ 6700 3950
Connection ~ 7350 3950
Connection ~ 8000 3950
Wire Wire Line
	4700 2900 4700 3150
Wire Wire Line
	4700 3150 5250 3150
Wire Wire Line
	5250 3150 5250 4500
Wire Wire Line
	5250 4500 8650 4500
Connection ~ 5350 4500
Connection ~ 6000 4500
Connection ~ 6700 4500
Connection ~ 7350 4500
Connection ~ 8000 4500
Wire Wire Line
	1950 1300 2000 1300
Wire Wire Line
	4850 1700 4900 1700
Wire Wire Line
	1950 2300 2000 2300
Wire Wire Line
	1500 1850 1550 1850
Wire Wire Line
	2500 1900 2550 1900
Wire Wire Line
	5600 1550 5650 1550
Wire Wire Line
	1550 1550 1550 1450
Wire Wire Line
	1550 1450 1300 1450
Wire Wire Line
	1300 1450 1300 1000
Wire Wire Line
	1300 1000 2550 1000
Wire Wire Line
	2550 1000 2550 1600
Connection ~ 2000 1000
Wire Wire Line
	2550 1600 4300 1600
Wire Wire Line
	4300 1600 4300 1400
Wire Wire Line
	4300 1400 4900 1400
Wire Wire Line
	4900 1400 4900 1250
Wire Wire Line
	4900 1250 5800 1250
Wire Wire Line
	5800 1250 5800 1950
Wire Wire Line
	5800 1950 5150 1950
Wire Wire Line
	5150 1950 5150 2900
Connection ~ 5650 1250
Wire Wire Line
	2000 2000 2000 1550
Wire Wire Line
	2000 1550 2550 1550
Connection ~ 2550 1550
Wire Wire Line
	1100 1850 1100 2200
Wire Wire Line
	1100 2200 1200 2200
Wire Wire Line
	1200 2200 1200 2950
Wire Wire Line
	1550 1300 1550 1400
Wire Wire Line
	1550 1400 1600 1400
Wire Wire Line
	1600 1400 1600 2000
Wire Wire Line
	1600 2000 1300 2000
Connection ~ 1300 2950
Wire Wire Line
	1400 2300 1550 2300
Connection ~ 1400 2950
Wire Wire Line
	1500 2450 2100 2450
Wire Wire Line
	2100 2450 2100 1900
Connection ~ 1500 2950
Wire Wire Line
	1600 2500 4450 2500
Wire Wire Line
	4450 2500 4450 1700
Connection ~ 1600 2950
Wire Wire Line
	5200 1550 5050 1550
Wire Wire Line
	5050 1550 5050 2550
Wire Wire Line
	5050 2550 1700 2550
Connection ~ 1700 2950
$Comp
L Conn_01x07 J2
U 1 1 5BD1143A
P 4700 2700
F 0 "J2" H 4700 3100 50  0000 C CNN
F 1 "Conn_01x07" H 4700 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 4700 2700 50  0001 C CNN
F 3 "" H 4700 2700 50  0001 C CNN
	1    4700 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 2900 5000 2900
$EndSCHEMATC
